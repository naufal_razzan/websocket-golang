var selectedChat = "general"

class Event{
    constructor(type, payload){
        this.type = type
        this.payload = payload
    }
}

class SendMessageEvent{
    constructor(message, from){
        this.message = message
        this.from = from
    }
}

class NewMessageEvent{
    constructor(message, from, sent){
        this.message = message
        this.from = from
        this.sent = sent
    }
}

class changeChatRoomEvent{
    constructor(name){
        this.name = name
    }
}

function routeEvent(event){
    if(event.type === undefined){
        alert('no type field in the event')
    }

    switch(event.type){
        case "new_message":
            const messageEvent = Object.assign(new NewMessageEvent, event.payload)
            appendChatMessage(messageEvent)
            break
        default:
            alert("unsupported message type")
            break
    }
}

function appendChatMessage(messageEvent){
    var date = new Date(messageEvent.sent)
    const formatMsg = `${date.toLocaleString()}: ${messageEvent.message}`

    textarea = Document.getElementById('chatmessages')
    textarea.innerHTML = textarea.innerHTML + '\n' + formatMsg
    textarea.scrollTop = textarea.scrollHeight
}

function sendEvent(eventName, payload){
    const event = new Event(eventName, payload)

    conn.send(JSON.stringify(event))
}

function changeChatRoom(){
    var newChat = document.getElementById("chatroom")
    if(newChat != null && newChat.value != selectedChat){
        selectedChat = newChat.value
        header = document.getElementById("chat-header").innerHTML = "Currently in chatroom: " + selectedChat
        
        let changeEvent = new changeChatRoomEvent(selectedChat)
        sendEvent("change_room", changeEvent)
        textarea = document.getElementById("chatmessages")
        textarea.innerHTML = `You changed room into: ${selectedChat}`
    }
    return false
}

function sendMessage(){
    var newMessaage = document.getElementById("messaage")
    if(newMessaage != null){
        let outgoingEvent = new SendMessageEvent(newMessaage.value, "naufal")
        sendEvent("send_message", outgoingEvent)
    }
    return false
}

async function login(){
    let formData = {
        "username": document.getElementById("username").value,
        "password": document.getElementById("password").value
    }
    
    try{
        const response = await fetch("login", {
            method: 'post',
            body: JSON.stringify(formData),
            mode: 'cors'
        })

        if(!response.ok){
            throw 'unauthorized'
        }

        const data = await response.json()
        connectWebSocket(data.otp)
    } catch(e){
        console.error(e)
        alert(e)
    }
}

function connectWebSocket(otp){
    if (window["WebSocket"]){
        console.log("supports websockets")
        let conn = new WebSocket("wss://" + document.location.host + "/ws/?otp=" + otp)
        
        conn.onopen = (evt) => {
            document.getElementById("connection-header").innerHTML = "Connected to Websocket: true"
        }

        conn.onclose = (evt) => {
            document.getElementById("connection-header").innerHTML = "Connected to Websocket: false"
        }
        
        conn.onmessage = function(evt){
            const eventData = JSON.parse(evt.data)

            const event = Object.assign(new Event, eventData)
            routeEvent(event)
        }
    }
    else{
        alert('Browser does not support websockets')
    }
}

window.onload = function() {
    document.getElementById("chatroom-selection").onsubmit = changeChatRoom
    document.getElementById("chatroom-message").onsubmit = sendMessage
    document.getElementById("login-form").onsubmit = login

    
}